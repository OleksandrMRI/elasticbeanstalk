package ua.shpp.demo1.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.shpp.demo1.service.StringService;

@RestController
@RequestMapping("firstController/")
public class FirstController {
    StringService stringService;
    @Autowired
    public FirstController(StringService stringService) {
        this.stringService = stringService;
    }

    @GetMapping("getHello/{name}")
    public String getSimpleString(@PathVariable String name){
        return stringService.produceGreeting(name);
    }
}
