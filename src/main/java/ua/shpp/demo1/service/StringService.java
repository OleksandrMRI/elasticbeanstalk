package ua.shpp.demo1.service;

public interface StringService {

    String produceGreeting(String name);
}
