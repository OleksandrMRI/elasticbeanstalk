package ua.shpp.demo1.service;

import org.springframework.stereotype.Service;

@Service
public class StringServiceImpl implements StringService {

    @Override
    public String produceGreeting(String name) {
        return "Hello from service, " + name;
    }
}
